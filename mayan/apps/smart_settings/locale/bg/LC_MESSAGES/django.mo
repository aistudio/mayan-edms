��          �      |      �     �     	               -     J     O  
   h     s  "   v     �     �     �     �     �  d   �     `     o     u     �     �  �  �  1   '     Y  6   p  2   �  K   �     &  C   -  %   q     �  <   �  $   �  ;   �  D   :       G   �  �   �     �     �  R   �  *   	     I	                                                                                    
         	    "%s" not a valid entry. Edit Edit setting: %s Edit settings Enter the new setting value. Name Namespace: %s, not found Namespaces No Overrided by environment variable? Setting count Setting namespaces Setting updated successfully. Settings Settings in namespace: %s Settings inherited from an environment variable take precedence and cannot be changed in this view.  Smart settings Value Value must be properly quoted. View settings Yes Project-Id-Version: Mayan EDMS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-10-17 10:59+0000
Last-Translator: Lyudmil Antonov <lantonov.here@gmail.com>
Language-Team: Bulgarian (http://www.transifex.com/rosarior/mayan-edms/language/bg/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: bg
Plural-Forms: nplurals=2; plural=(n != 1);
 &quot;%s&quot; не е валиден запис. Редактиране Редактиране на настройката: %s Редактиране на настройките Въведете новата стойност на настройката. Име Именно пространство: %s, не е намерено Именни пространства Не Отменена от системна променлива? Брой на настройките Задаване на именни пространства Настройката е актуализирана успешно. Настройки Настройки в именното пространството: %s Настройките, наследени от системна променлива имат предимство и не могат да бъдат променени в този изглед. Умни настройки Стойност Стойността трябва да бъде правилно цитирана. Преглед на настройките Да 